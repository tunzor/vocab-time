package com.tunzor.vocabtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView practiceB, wordsB, settingsB, setsB, tutorial = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        practiceB = (TextView) findViewById(R.id.practiceButton);
        practiceB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), PracticeActivity.class);
                startActivity(in);
            }
        });
        
        wordsB = (TextView) findViewById(R.id.wordsButton);
        wordsB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), WordsActivityMain.class);
                startActivity(in);
            }
        });

        setsB = (TextView) findViewById(R.id.setsButton);
        setsB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), SetsActivityMain.class);
                startActivity(in);
            }
        });

        tutorial = (TextView) findViewById(R.id.tutorialButton);
        tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), TutorialActivity.class);
                startActivity(in);
            }
        });
        
        /*settingsB = (TextView) findViewById(R.id.settingsButton);
        settingsB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(in);
            }
        });*/
    }
}
