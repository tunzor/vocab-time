package com.tunzor.vocabtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WordsActivityMain extends AppCompatActivity {

    Button addPair, deleteAllPairs, showAllPairs = null;
    EditText pair1, pair2 = null;
    ListView pairsList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words_activity_main);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if(b.getString("pairUpdated") != null) {
                String message = b.getString("pairUpdated");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } else if(b.getString("pairDeleted") != null) {
                String message = b.getString("pairDeleted");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        addPair = (Button) findViewById(R.id.addPairButton);
        deleteAllPairs = (Button) findViewById(R.id.deleteAllPairs);
        pairsList = (ListView) findViewById(R.id.listView1);

        pairsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                int itemId = Integer.parseInt(item.substring(0, item.indexOf('|')).trim());
                String firstWord = item.substring(item.indexOf('|')+2, item.lastIndexOf('|')-1);
                String secondWord = item.substring(item.lastIndexOf('|')+2, item.length());
                Intent intent = new Intent(getApplicationContext(), WordsEditActivity.class);
                intent.putExtra("pairId", itemId);
                intent.putExtra("firstWord", firstWord);
                intent.putExtra("secondWord", secondWord);
                startActivity(intent);
                finish();
            }
        });

        addPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPair(v);
                viewAllPairs(findViewById(R.id.listView1), false);
            }
        });

        deleteAllPairs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAllPairs(v);
                ListView et = (ListView) findViewById(R.id.listView1);
                et.setAdapter(null);
                viewAllPairs(findViewById(R.id.listView1), false);
            }
        });

        viewAllPairs(pairsList, false);
    }

    public void createPair(View view) {
        TextView firstWord = (TextView) findViewById(R.id.wordPair1);
        TextView secondWord = (TextView) findViewById(R.id.wordPair2);

        Pair newPair = new Pair(firstWord.getText().toString().trim(), secondWord.getText().toString().trim());
        DBHelper db = new DBHelper(this);
        if(!"".equalsIgnoreCase(firstWord.getText().toString().trim()) && !"".equalsIgnoreCase(secondWord.getText().toString().trim())) {
            if (db.doesPairExist(newPair)) {
                Toast.makeText(this, "That pair already exists.", Toast.LENGTH_LONG).show();
            } else {
                boolean insertSuccess = db.insertPair((newPair));
                if (insertSuccess) {
                    Toast.makeText(this, newPair.getFirstWord() + ", " + newPair.getSecondWord() + " pair added", Toast.LENGTH_LONG).show();
                    firstWord.setText("");
                    secondWord.setText("");
                }
            }
        } else {
            Toast.makeText(this, "Empty words are not allowed.", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteAllPairs(View view) {
        DBHelper db = new DBHelper(this);
        if(db.deleteAllPairs()) {
            Toast.makeText(this, "All pairs have been deleted.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "No pairs currently exist.", Toast.LENGTH_LONG).show();
        }
    }

    public void viewAllPairs(View view, boolean showToast) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getAllPairs();
        if(list.size() == 0) {
            if(showToast) {
                Toast.makeText(this, "There aren't any pairs to retrieve.", Toast.LENGTH_LONG).show();
            }
        } else {
            ListView obj;
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

            obj = (ListView)findViewById(view.getId());
            obj.setAdapter(adapter);
        }
    }
}
