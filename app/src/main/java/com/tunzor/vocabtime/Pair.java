package com.tunzor.vocabtime;

/**
 * Created by Anthony on 2015-01-01.
 */
public class Pair {
    private String firstWord;
    private String secondWord;
    private String set;
    private String collection;
    private int id;

    public Pair() {
        this.id = 0;
        this.firstWord = null;
        this.secondWord = null;
        this.set = null;
        this.collection = null;
    }
    public Pair(int id, String first, String second) {
        this.id = id;
        this.firstWord = first;
        this.secondWord = second;
        this.set = null;
        this.collection = null;
    }

    public Pair(String first, String second) {
        this.id = 0;
        this.firstWord = first;
        this.secondWord = second;
        this.set = null;
        this.collection = null;
    }

    public Pair(String first, String second, String set) {
        this.id = 0;
        this.firstWord = first;
        this.secondWord = second;
        this.set = set;
        this.collection = null;
    }

    public Pair(String first, String second, String set, String collection) {
        this.id = 0;
        this.firstWord = first;
        this.secondWord = second;
        this.set = set;
        this.collection = collection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstWord() {
        return firstWord;
    }

    public void setFirstWord(String firstWord) {
        this.firstWord = firstWord;
    }

    public String getSecondWord() {
        return secondWord;
    }

    public void setSecondWord(String secondWord) {
        this.secondWord = secondWord;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public String[] getPair() {
        String[] pair = new String[2];
        pair[0] = this.firstWord;
        pair[1] = this.secondWord;
        return pair;
    }
}