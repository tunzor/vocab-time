package com.tunzor.vocabtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class SetsActivityMain extends AppCompatActivity {

    Button addSet, deleteSets = null;
    EditText setName, setDesc = null;
    ListView setsList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sets_activity);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if(b.getString("setUpdated") != null) {
                String message = b.getString("setUpdated");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            } else if(b.getString("setDeleted") != null) {
                String message = b.getString("setDeleted");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        addSet = (Button) findViewById(R.id.addSetButton);
        deleteSets = (Button) findViewById(R.id.deleteAllSetsButton);
        setName = (EditText) findViewById(R.id.setName);
        setDesc = (EditText) findViewById(R.id.setDesc);

        setsList = (ListView) findViewById(R.id.setsListView);

        setsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                int setId = Integer.parseInt(item.substring(0, item.indexOf('|')).trim());
                String setName = item.substring(item.indexOf('|')+2, item.lastIndexOf('|')-1);
                String setDesc = item.substring(item.lastIndexOf('|')+2, item.length());
                Intent intent = new Intent(getApplicationContext(), SetsEditActivity.class);
                intent.putExtra("setId", setId);
                intent.putExtra("setName", setName);
                intent.putExtra("setDesc", setDesc);
                startActivity(intent);
                finish();
                //Toast.makeText(getApplicationContext(), "ID: "+itemId+", FIRST: "+firstWord+", SECOND:"+secondWord, Toast.LENGTH_LONG).show();

            }
        });

        addSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSet();
                viewAllSets(findViewById(R.id.setsListView), true);
            }
        });

        deleteSets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSets();
                ListView et = (ListView) findViewById(R.id.setsListView);
                et.setAdapter(null);
                viewAllSets(findViewById(R.id.setsListView), false);
            }
        });

        viewAllSets(findViewById(R.id.setsListView), false);
    }

    public void viewAllPairs(View view) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getAllPairs();
        if(list.size() == 0) {
            Toast.makeText(this, "There aren't any pairs to retrieve.", Toast.LENGTH_LONG).show();
        } else {
            ListView obj;
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

            obj = (ListView)findViewById(view.getId());
            obj.setAdapter(adapter);
        }
    }

    public void createSet() {
        setName = (EditText) findViewById(R.id.setName);
        setDesc = (EditText) findViewById(R.id.setDesc);
        String setNameString = setName.getText().toString().trim();
        String setDescString = setDesc.getText().toString().trim();

        DBHelper db = new DBHelper(this);
        if(!"".equalsIgnoreCase(setNameString) && !"".equalsIgnoreCase(setDescString)) {
            if (db.doesSetExist(setNameString)) {
                Toast.makeText(this, "That set already exists.", Toast.LENGTH_LONG).show();
            } else {
                boolean insertSuccess = db.insertSet(setNameString, setDescString);
                if (insertSuccess) {
                    Toast.makeText(this, setNameString + " set added", Toast.LENGTH_LONG).show();
                    setName.setText("");
                    setDesc.setText("");
                }
            }
        } else {
            Toast.makeText(this, "Empty sets are not allowed.", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteSets() {
        DBHelper db = new DBHelper(this);
        if(db.deleteAllSets()) {
            Toast.makeText(this, "All sets have been deleted.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "No sets currently exist.", Toast.LENGTH_LONG).show();
        }
    }

    public void viewAllSets(View view, boolean showToast) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getAllSets();
        if(list.size() == 0) {
            if(showToast) {
                Toast.makeText(this, "There aren't any sets to retrieve.", Toast.LENGTH_LONG).show();
            }
        } else {
            ListView obj;
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

            obj = (ListView)findViewById(view.getId());
            obj.setAdapter(adapter);
        }
    }

}
