package com.tunzor.vocabtime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class PracticeActivity extends AppCompatActivity {

    ListView setsList, setPairsList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        setsList = (ListView) findViewById(R.id.practiceSetsListView);
        setPairsList = (ListView) findViewById(R.id.practiceSetPairsListView);

        viewAllSets(setsList, false);

        setsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                int setId = Integer.parseInt(item.substring(0, item.indexOf('|')).trim());
                viewSetPairs(setPairsList, setId);
            }
        });
    }

    public void viewAllSets(View view, boolean showToast) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getAllSets();
        if(list.size() == 0) {
            if(showToast) {
                Toast.makeText(this, "There aren't any sets to retrieve.", Toast.LENGTH_LONG).show();
            }
        } else {
            ListView obj;
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

            obj = (ListView)findViewById(view.getId());
            obj.setAdapter(adapter);
        }
    }

    public void viewSetPairs(View view, int setId) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getSetPairs(setId);
        if(list.size() == 0) {
            setPairsList.setAdapter(null);
            Toast.makeText(this, "There aren't any pairs associated with that set.", Toast.LENGTH_LONG).show();
        } else {
            ListView obj;
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

            obj = (ListView)findViewById(view.getId());
            obj.setAdapter(adapter);
        }
    }
}
