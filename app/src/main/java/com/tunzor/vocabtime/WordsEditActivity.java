package com.tunzor.vocabtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WordsEditActivity extends AppCompatActivity {

    Button editPair, cancel, deletePair = null;
    String word1, word2 = null;
    EditText pair1, pair2 = null;
    int pairId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words_edit);

        editPair = (Button) findViewById(R.id.editPairButton);
        cancel = (Button) findViewById(R.id.cancelPairEdit);
        deletePair = (Button) findViewById(R.id.deletePair);
        pair1 = (EditText) findViewById(R.id.editWordPair1);
        pair2 = (EditText) findViewById(R.id.editWordPair2);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            pairId = b.getInt("pairId");
            word1 = b.getString("firstWord");
            word2 = b.getString("secondWord");
            pair1.setText(word1);
            pair2.setText(word2);
        }

        editPair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPair(pairId, pair1.getText().toString().trim(), pair2.getText().toString().trim());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WordsActivityMain.class);
                startActivity(intent);
                finish();
            }
        });

        deletePair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePair(pairId, pair1.getText().toString().trim(), pair2.getText().toString().trim());
            }
        });
    }

    public void editPair(int i, String first, String second) {
        DBHelper db = new DBHelper(this);
        if (!"".equalsIgnoreCase(first.trim()) && !"".equalsIgnoreCase(second.trim())) {
            if (db.editPair(i, first, second)) {
                //Toast.makeText(this, "Updated pair: "+first+" | "+second, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), WordsActivityMain.class);
                intent.putExtra("pairUpdated", "Updated pair: "+first+" | "+second);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Shit... Something bad happened.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Empty words are not allowed.", Toast.LENGTH_LONG).show();
        }
    }

    public void deletePair(int i, String first, String second) {
        DBHelper db = new DBHelper(this);
        if (db.deletePair(Integer.toString(i)) && db.deleteAssociationByPair(i)) {
            Intent intent = new Intent(getApplicationContext(), WordsActivityMain.class);
            intent.putExtra("pairDeleted", "Deleted pair: "+first+" | "+second);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Shit... Something bad happened.", Toast.LENGTH_LONG).show();
        }
    }
}
