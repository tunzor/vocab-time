package com.tunzor.vocabtime;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    Bundle savedBundle = null;
    boolean redText = false;
    boolean blueBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        savedBundle = savedInstanceState;
        if(savedInstanceState != null) {
            redText = savedInstanceState.getBoolean("redText");
            blueBack = savedInstanceState.getBoolean("blueBack");
        }
        setContentView(R.layout.activity_settings);

    }

    public void setting1Clicked(View v) {
        Toast.makeText(SettingsActivity.this, "Change to setting 1", Toast.LENGTH_SHORT).show();
        /*if(redText) {
            redText = false;
        } else {
            redText = true;
        }
        saveSettings();*/
    }

    public void setting2Clicked(View v) {
        Toast.makeText(SettingsActivity.this, "Change to setting 2", Toast.LENGTH_SHORT).show();
        /*if(blueBack) {
            blueBack = true;
        } else {
            blueBack = false;
        }
        saveSettings();*/
    }

    public void setting3Clicked(View v) {
        Toast.makeText(SettingsActivity.this, "Change to setting 3", Toast.LENGTH_SHORT).show();
    }

    private void saveSettings() {
        savedBundle.putBoolean("redText", redText);
        savedBundle.putBoolean("blueBack", blueBack);
    }
}
