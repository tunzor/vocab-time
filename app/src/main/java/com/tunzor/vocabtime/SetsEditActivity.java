package com.tunzor.vocabtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class SetsEditActivity extends AppCompatActivity {

    Button editSet, cancel, deleteSet = null;
    String setName, setDesc = null;
    EditText setNameEdit, setDescEdit = null;
    ListView setsPairsList = null;
    int setId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sets_edit);

        editSet = (Button) findViewById(R.id.editSetButton);
        cancel = (Button) findViewById(R.id.cancelSetEdit);
        deleteSet = (Button) findViewById(R.id.deleteSet);
        setNameEdit = (EditText) findViewById(R.id.editSetName);
        setDescEdit = (EditText) findViewById(R.id.editSetDesc);
        setsPairsList = (ListView) findViewById(R.id.setsEditPairsList);

        populatePairsList(setsPairsList);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            setId = b.getInt("setId");
            setName = b.getString("setName");
            setDesc = b.getString("setDesc");
            setNameEdit.setText(setName);
            setDescEdit.setText(setDesc);
        }

        editSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSet(setId, setNameEdit.getText().toString().trim(), setDescEdit.getText().toString().trim());
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SetsActivityMain.class);
                startActivity(intent);
                finish();
            }
        });

        deleteSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSet(setId, setNameEdit.getText().toString().trim());
            }
        });

        setsPairsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DBHelper db = new DBHelper(getApplicationContext());
                String item = parent.getItemAtPosition(position).toString();
                int itemId = Integer.parseInt(item.substring(0, item.indexOf('|')).trim());
                String firstWord = item.substring(item.indexOf('|') + 2, item.lastIndexOf('|') - 1);
                String secondWord = item.substring(item.lastIndexOf('|') + 2, item.length());
                //Toast.makeText(getApplicationContext(), "SET ID: "+setId+". PAIR ID: "+itemId, Toast.LENGTH_LONG).show();
                if(db.doesAssociationExist(setId, itemId)) {
                    Toast.makeText(getApplicationContext(), "That association already exists.", Toast.LENGTH_LONG).show();
                } else {
                    if (db.insertAssociation(setId, itemId)) {
                        Toast.makeText(getApplicationContext(), "Association successful. Added pair " + firstWord + " | " + secondWord + " to set " + setName, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Something bad happened. No association created.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        setsPairsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), "You long pressed "+position, Toast.LENGTH_SHORT).show();
                DBHelper db = new DBHelper(getApplicationContext());
                String item = parent.getItemAtPosition(position).toString();
                int pairId = Integer.parseInt(item.substring(0, item.indexOf('|')).trim());
                String firstWord = item.substring(item.indexOf('|') + 2, item.lastIndexOf('|') - 1);
                String secondWord = item.substring(item.lastIndexOf('|') + 2, item.length());
                if(db.deleteAssociation(setId, pairId)) {
                    Toast.makeText(getApplicationContext(), "Removed the association of: "+firstWord+" | "+secondWord+" from set "+setName, Toast.LENGTH_LONG).show();
                    populatePairsList(setsPairsList);
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    public void editSet(int i, String name, String desc) {
        DBHelper db = new DBHelper(this);
        if (!"".equalsIgnoreCase(name.trim()) && !"".equalsIgnoreCase(desc.trim())) {
            if (db.editSet(i, name, desc)) {
                Intent intent = new Intent(getApplicationContext(), SetsActivityMain.class);
                intent.putExtra("setUpdated", "Updated set with name: " + name);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Shit... Something bad happened.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Empty sets are not allowed.", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteSet(int i, String name) {
        DBHelper db = new DBHelper(this);
        if (db.deleteSet(Integer.toString(i)) && db.deleteAssociationBySet(i)) {
            Intent intent = new Intent(getApplicationContext(), SetsActivityMain.class);
            intent.putExtra("setDeleted", "Deleted set with name: " + name);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Shit... Something bad happened.", Toast.LENGTH_LONG).show();
        }
    }

    public void populatePairsList(View v) {
        DBHelper db = new DBHelper(this);
        ArrayList list = db.getAllPairs();
        ListView obj;
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);

        obj = (ListView) findViewById(v.getId());
        obj.setAdapter(adapter);
    }
}
