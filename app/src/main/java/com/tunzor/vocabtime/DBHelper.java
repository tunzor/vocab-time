package com.tunzor.vocabtime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Anthony on 2015-01-02.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "VocaberDB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE pairs"+
                "(_id INTEGER primary key, firstword TEXT, secondword TEXT);");
        db.execSQL("CREATE TABLE sets" +
                "(_id INTEGER primary key, name TEXT, desc TEXT);");
        db.execSQL("CREATE TABLE sets_pairs_bridge" +
                "(set_id INTEGER, pair_id INTEGER, primary key (set_id, pair_id));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL("DROP IF EXISTS TABLE pairs");
        onCreate(db);
    }

    public boolean insertPair(Pair pair) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("firstword", pair.getFirstWord());
        values.put("secondword", pair.getSecondWord());

        return db.insert("pairs", null, values) != -1;
    }

    public boolean insertSet(String setName, String desc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("name", setName);
        values.put("desc", desc);

        return db.insert("sets", null, values) != -1;
    }

    public ArrayList getAllPairs() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT * FROM pairs ORDER BY firstword ASC", null);
        res.moveToFirst();
        res.moveToFirst();
        while(!res.isAfterLast()) {
            list.add(res.getString(res.getColumnIndex("_id")) + " | " + res.getString(res.getColumnIndex("firstword")) + " | " + res.getString(res.getColumnIndex("secondword")));
            res.moveToNext();
        }
        res.close();
        return list;
    }

    public ArrayList<Pair> getAllPAIRS() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Pair> list = new ArrayList<Pair>();
        Cursor res = db.rawQuery("SELECT * FROM pairs ORDER BY firstword ASC", null);
        res.moveToFirst();
        res.moveToFirst();
        while(!res.isAfterLast()) {
            list.add(new Pair(res.getInt(res.getColumnIndex("_id")), res.getString(res.getColumnIndex("firstword")), res.getString(res.getColumnIndex("secondword"))));
            res.moveToNext();
        }
        res.close();
        return list;
    }

    public ArrayList getAllSets() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT * FROM sets ORDER BY name ASC", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            list.add(res.getString(res.getColumnIndex("_id")) + " | " + res.getString(res.getColumnIndex("name")) + " | " + res.getString(res.getColumnIndex("desc")));
            res.moveToNext();
        }
        res.close();
        return list;
    }

    public boolean deletePair(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("pairs","_id=?",new String[]{id}) > 0;
    }

    public boolean deleteAllPairs() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("pairs", null, null) > 0;
    }

    public boolean deleteSet(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("sets","_id=?",new String[]{id}) > 0;
    }

    public boolean deleteAllSets() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("sets", null, null) > 0;
    }

    public boolean doesPairExist(Pair pair) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM pairs", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            if(pair.getFirstWord().trim().equalsIgnoreCase(res.getString(res.getColumnIndex("firstword"))) &&
                    pair.getSecondWord().trim().equalsIgnoreCase(res.getString(res.getColumnIndex("secondword")))) {
                res.close();
                return true;
            }
            res.moveToNext();
        }
        res.close();
        return false;
    }

    public boolean editPair(int id, String first, String second) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE pairs" +
                " SET firstWord='"+first+
                "', secondWord='"+second+
                "' WHERE _id="+id+";");
        return true;
    }

    public boolean editSet(int id, String name, String desc) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE sets" +
                " SET name='"+name+
                "', desc='"+desc+
                "' WHERE _id="+id+";");
        return true;
    }

    public boolean doesSetExist(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT name FROM sets", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            if(name.trim().equalsIgnoreCase(res.getString(res.getColumnIndex("name")))) {
                res.close();
                return true;
            }
            res.moveToNext();
        }
        res.close();
        return false;
    }

    public boolean insertAssociation(int setId, int pairId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("set_id", setId);
        values.put("pair_id", pairId);

        return db.insert("sets_pairs_bridge", null, values) != -1;
    }

    public boolean doesAssociationExist(int setId, int pairId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM sets_pairs_bridge", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            if(setId == (res.getInt(res.getColumnIndex("set_id"))) && pairId == res.getInt(res.getColumnIndex("pair_id"))) {
                res.close();
                return true;
            }
            res.moveToNext();
        }
        res.close();
        return false;
    }

    public ArrayList getSetPairs(int setId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT firstword, secondword " +
                " FROM pairs" +
                " LEFT JOIN sets_pairs_bridge" +
                " ON sets_pairs_bridge.pair_id=pairs._id" +
                " WHERE sets_pairs_bridge.set_id=" +setId+
                " ORDER BY pairs.firstword;", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            list.add(res.getString(res.getColumnIndex("firstword")) + " | " + res.getString(res.getColumnIndex("secondword")));
            res.moveToNext();
        }
        res.close();
        return list;
    }

    public ArrayList<Pair> getSetPAIRS(int setId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList list = new ArrayList();
        Cursor res = db.rawQuery("SELECT firstword, secondword " +
                " FROM pairs" +
                " LEFT JOIN sets_pairs_bridge" +
                " ON sets_pairs_bridge.pair_id=pairs._id" +
                " WHERE sets_pairs_bridge.set_id=" +setId+
                " ORDER BY pairs.firstword;", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            list.add(new Pair(res.getString(res.getColumnIndex("firstword")), res.getString(res.getColumnIndex("secondword"))));
            res.moveToNext();
        }
        res.close();
        return list;
    }

    public boolean deleteAssociationByPair(int pairId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("sets_pairs_bridge","pair_id=?",new String[]{Integer.toString(pairId)}) > 0;
    }

    public boolean deleteAssociationBySet(int setId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("sets_pairs_bridge","set_id=?",new String[]{Integer.toString(setId)}) > 0;
    }

    public boolean deleteAssociation(int setId, int pairId) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("sets_pairs_bridge","set_id=? and pair_id=?",new String[]{Integer.toString(setId), Integer.toString(pairId)}) > 0;
    }

}